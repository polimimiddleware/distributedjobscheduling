package utility;

import job.Job;
import queue.PermanentQueue;

public class SpamJob {
	public static void main(String args[]) throws InterruptedException {
		PermanentQueue jobs = new PermanentQueue("TestQueue");
		for(int i =0;i<100;i++){
			String s=""+i;
			new Thread(){
				public void run(){
					System.out.println("Thread i: "+s);
					Job j= new Job(null);
					j.setId(s);
				    if(!jobs.add(j)){
				    	System.out.println("Error");
				    };
				}
			}.start();
			System.out.println("Size: "+jobs.getPendingJobsSize());
		}
		
	} 
}
