package queue;

import job.IJob;
import job.Result;

public interface IPermanentQueue {
	boolean add(IJob j);
	IJob getNextJob();
	boolean finalizeJob(String id , Result r);
	IJob getJobByID(String id);
	String printJobStatus();
	
	int getPendingJobsSize();
	int getCompletedJobsSize();
	boolean restoreQueue();
}
